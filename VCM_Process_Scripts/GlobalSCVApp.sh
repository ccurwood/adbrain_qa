#!/bin/bash
#
#This script executes the GlobalSCVApp function within VCM

#NOTE
#Actual JSON data will need to be edited for specifics
#prior to execution.

curl --verbose -i -H "X-Adbrain-AuthToken:5CFC02F4-8512-47F1-BEBA-1E36FBCF2896" -H "Content-Type:application/json" POST http://ab-vcm-prod.cloudapp.net:3320/vcm/app-descriptions/ -d '{Name:"GlobalScvApp",IsRecurring:false,Input:"{\"InputPaths\":[ \"s3://adbrain-graph-prod/private/infectious/archive/agg-msresults/1/2016-08-18_2016-09-18\" ],\"GraphName\":\"infectious\"}",CronExpression:"/1***",RunNow:true}'
